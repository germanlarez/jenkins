# Jenkins

## Install
`docker run -d --name jenkins -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts`

### Initial Admin password can be found in either the host `/var/lib/docker/volumes/jenkins_home/_data/secrets/initialAdminPassword` or into the container at `var/jenkins_home/secrets` directory. This is done to persists data as credentials, jobs and other configurations on host so we can instantiate another container with the same volume attached and we are going to have everything in the new one.

### To get into the container as the root user in order to install tools or plugins:
`docker exec -u 0 -it ${CONTAINER ID OR NAME} bash`

### You may need to know wich version of linux your using to know how to install your tools
`cat /etc/issue` 
 
### Then go to [Node install instructions page](https://github.com/nodesource/distributions/blob/master/README.md) for your version

### Make the host's docker installation available into Jenkins container:

we are mounting 3 volumes for that the first for data and configs, second one for docker sock and the third one for docker runtime 
`docker run -d --name jenkins -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):/usr/bin/docker jenkins/jenkins:lts`

Other thing we need to address is to give jenkins user permission on docker.sock on host:
login into the container as root
`docker exec -it -u 0 jenkins bash`
give permissions for jenkins user to the docker.sock file
`chmod 666 /var/run/docker.sock` 

And we are all set to use docker commands on our jenkins.

